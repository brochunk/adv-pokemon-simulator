#pragma once

#include <string>
#include "Pokemon.h"
#include <iostream>
#include <conio.h>
#include <vector>

using namespace std;

class Player
{
public:
	Player(string playerName, char gender, int coordinates[2], vector<string> pokemonCollection);
	string playerName;
	char gender;
	int coordinates[2] = { 0,0 };
	vector<Pokemon> pokemonCollection;

	
};
