#include <string>
#include <iostream>
#include <conio.h>

#include "Location.h"

using namespace std;

Location::Location(string locationName,  int xCoordinate, int yCoordinate, bool encounterBoy)
{
	Location* littleroot = new Location("Littleroot Town", 0, 0, 1);
	Location* oldale = new Location("Oldale Town", 0, 3, 1);
	Location* petalburg = new Location("Petalburg City", -4, 3, 1);
	Location* petalburgWoods = new Location("PetalBurg Woods", -7, 4, 0);
	Location* rustboro = new Location("Rustboro City", -7, 9, 1);
	Location* meteorFalls = new Location("Meteor Falls", -5, 17, 0);
	Location* fallarbor = new Location("Fallarbor Town", -1, 20, 1);
	Location* gnarledDen = new Location("Gnarled Den", -1, 16, 0);
	Location* lavaridge = new Location("Lavaridge Town", 2, 15, 1);
	Location* fieryPath = new Location("Fiery Path", 5, 17, 0);
	Location* mauville = new Location("Mauville City", 6, 10, 1);
	Location* verdanturf = new Location("Venandurf Town", 0, 10, 1);
	Location* fortree = new Location("Fortree City", 22, 19, 1);
	Location* safariZone = new Location("Safari Zone", 28, 17, 1);
	Location* lilycove = new Location("Lilicove City", 32, 16 ,1);
	Location* sootopolis = new Location("Sootopolis City", 35, 8, 1);
	Location* mossedeep = new Location("Mosdeep City", 39, 15, 1);
	Location* shoalCave = new Location("Shoal Cave", 40, 19, 0);
	Location* everGrande = new Location("Ever Grande City", 42, 7, 1);
	Location* pacifidlog = new Location("Pacifidlog Town", 29, 3, 1);
	Location* slateport = new Location("Slateport City", 6, 3, 1);
	Location* dewford = new Location("Dewford Town", -7, -7, 1);

};