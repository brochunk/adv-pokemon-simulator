#pragma once

#include <string>
#include <iostream>
#include <conio.h>

using namespace std;

class Pokemon
{
public:
	Pokemon(string pokemonName, int baseHp, int hp, int level, int baseDamage, int exp, int expToNextLevel);
	string pokemonName;
	int baseHp;
	int hp;
	int level;
	int baseDamage;
	int exp;
	int expToNextLevel;
};