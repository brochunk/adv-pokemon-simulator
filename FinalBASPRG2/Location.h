#pragma once

#include <string>
#include <iostream>
#include <conio.h>

using namespace std;

class Location
{
public:
	Location(string locationName, int xCoordinate, int yCoordinate, bool pokemonEncounter);
	string locationName;
	bool pokemonEncounter; //determines whether area is safe/not
	int xCoordinate;
	int yCoordinate;
};